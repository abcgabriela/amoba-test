@extends('layouts.app')

@section('content')

<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/components/navbar/">

    <section>  
        
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="../img/image989.png" class="d-block w-100" alt="...">
      <div class="carousel-caption-g">
          <h1 class="slide-title poppins"> <strong>BOLD</strong> & <strong>BEAUTIFUL</strong></h1>
        </div>
    </div>
    <div class="carousel-item">
      <img src="../img/image988.png" class="d-block w-100" alt="...">
      <div class="carousel-caption-g">
          <h1 class="slide-title poppins"> <strong>BOLD</strong> & <strong>BEAUTIFUL</strong></h1>
        </div>
    </div>
    <div class="carousel-item">
      <img src="../img/image987.png" class="d-block w-100" alt="...">
      <div class="carousel-caption-g">
      <h1 class="slide-title poppins"> <strong>BOLD</strong> & <strong>BEAUTIFUL</strong></h1>
        </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</section> 




    <!-- SECTION 2: whats we do? -->
<section class="bg-white pl-5 pr-5 pb-5 pt-5">
    <div class="row pb-3">
        <div class="col-lg-4">
            <hr class="featurette-divider">
        </div>
        <div class="col-lg-4 text-center">
            <h2><strong>WHAT</strong> WE DO?</h2>
        </div>
        <div class="col-lg-4">
            <hr class="featurette-divider"> 
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <div class="text-center">
                <img class="img-fluid" width="95" height="95"
                    data-src="../img/path1021.png"
                    src="../img/path1021.png">
                <h6><strong>WEB APPLICATIONS</strong></h6>
            </div>
            <p class="parrafos">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. </p>
            <a href="">
                <p class="azul"><strong>Read</strong> More</p>
            </a>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-3">
            <div class="text-center">
                <img class="img-fluid" width="95" height="95"
                    data-src="../img/g1097.png"
                    src="../img/g1097.png">
                <h6><strong>CLOUD HOSTING</strong></h6>
            </div>
            <p class="parrafos">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. </p>
            <a href="">
                <p class="azul"><strong>Read</strong> More</p>
            </a>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-3">
            <div class="text-center">
                <img class="img-fluid" width="95" height="95"
                    data-src="../img/image1087.png"
                    src="../img/image1087.png">
                <h6><strong>SOCIAL APPS</strong></h6>
            </div>
            <p class="parrafos">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. </p>
            <a href="">
                <p class="azul"><strong>Read</strong> More</p>
            </a>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-3">
            <div class="text-center">
                <img class="img-fluid" width="95" height="95"
                    data-src="../img/image1085.png"
                    src="../img/image1085.png">
                <h6><strong>SMART DESING</strong></h6>
            </div>
            <p class="parrafos">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. </p>
            <a href="">
                <p class="azul"><strong>Read</strong> More</p>
            </a>
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
    </section>

<!-- SECTION 3: recent work? -->


<section class="bg-gray-100 border-gray-200 pl-5 pr-5 pb-5 pt-5">
    <div class="row pb-3">
        <div class="col-lg-4">
        <hr class="featurette-divider">
        </div>
        <div class="col-lg-4 text-center">
            <h2><strong>RECENT</strong> WORK?</h2>
        </div>
        <div class="col-lg-4">
        <hr class="featurette-divider">
        </div>
        </div>
        <div class="row ">
        <div class="col-lg-6 pl-5">
            <br><br><br><br>
        <div id="carouselExampleIndicators" class="carousel slide pl-5" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="../img/image917-6.png" class="size" alt="...">
    </div>
    <div class="carousel-item">
      <img src="../img/image989-6.png" class="size" alt="...">
    </div>
  </div>
</div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-0">
                    <span class="iconify azul" data-icon="mdi-twitter"></span>
                </div>
                <div class="col">
                    <h6 class="azul"><strong>SOCIAL APPS DEVELOPMENT</strong></h6>
                    <p class="text-sm pb-2 parrafos">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula
                        porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-0">
                <span class="material-icons azul">favorite</span>
                </div>
                <div class="col">
                    <h6 class="azul"><strong>APPS THAT YOUR CLIENTS ARE GOIND TO LOVE</strong></h6>
                    <p class="text-sm pb-2 parrafos">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula
                        porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-0">
                <span class="material-icons azul">settings</span>
                </div>
                <div class="col">
                    <h6 class="azul"><strong>INOVATIONS THAT YOU TO THE NEXT LEVEL</strong></h6>
                    <p class="text-sm pb-2 parrafos">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula
                        porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-0">
                <span class="material-icons azul">eco</span>
                </div>
                <div class="col">
                    <h6 class="azul"><strong>ENVIROMENTAL FRIENDLY DESING</strong></h6>
                    <p class="text-sm pb-2 parrafos">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula
                        porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-0">
                <span class="material-icons azul">edit</span>
                </div>
                <div class="col">
                    <h6 class="azul"><strong>DESIGN THAT HASSOME MAGIC INIT</strong></h6>
                    <p class="text-sm pb-2 parrafos">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula
                        porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</p>
                </div>
            </div>

            </div>
    </div>
</section>

<!-- SECTION 4: last news? -->
<section class="bg-white pl-5 pr-5 pb-5 pt-5">
    <div class="row pb-3">
        <div class="col-lg-4">
        <hr class="featurette-divider">
        </div>
        <div class="col-lg-4 text-center">
            <h2><strong>LAST</strong> NEWS?</h2>
        </div>
        <div class="col-lg-4">
        <hr class="featurette-divider">
        </div>
    </div>
    

    <div class="row">
       
        <div class="col-lg-4">
        <div class="row">
                <div class="col-3">
                <img class="img-fluid" width="58" height="58"
                    data-src="../img/image861.png"
                    src="../img/image861.png">
                    <p><small>12 DEC 2012</small></p>
                </div>
                <div class="col">
                    <h6 class="azul"><strong>A NORMAL POST</strong></h6>
                    <p class="text-sm pb-2 parrafos">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula
                        porta felis euismod semper.</p>
                </div>
            </div>
        </div>
       
        <div class="col-lg-4">
        <div class="row">
                <div class="col-3">
                <img class="img-fluid" width="58" height="58"
                    data-src="../img/path925.png"
                    src="../img/path925.png">
                    <p><small>12 DEC 2012</small></p>
                </div>
                <div class="col">
                    <h6 class="azul"><strong>LINK POST </strong></h6>
                    <p class="text-sm pb-2 parrafos">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula
                        porta felis euismod semper.</p>
                </div>
            </div>
        </div>
       
        <div class="col-lg-4">
        <div class="row">
                <div class="col-3">
                <img class="img-fluid" width="58" height="58"
                    data-src="../img/image861-6-0.png"
                    src="../img/image861-6-0.png">
                    <p><small>12 DEC 2012</small></p>
                </div>
                <div class="col">
                    <h6 class="azul"><strong>VIDEO POST</strong></h6>
                    <p class="text-sm pb-2 parrafos">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula
                        porta felis euismod semper.</p>
                </div>
            </div>
        </div>
        
    </div><!-- /.row -->
</section>

<!-- SECTION 5: CLIENTS WE LOVE -->
<section class="bg-gray-100 border-gray-200 pl-5 pr-5 pb-5 pt-5">
<div class="row pb-3">
        <div class="col-lg-4">
        <hr class="featurette-divider">
        </div>
        <div class="col-lg-4 text-center">
            <h2><strong>CLIENTS</strong> WE LOVE</h2>
        </div>
        <div class="col-lg-4">
        <hr class="featurette-divider">
        </div>
        </div>
</div>

</section>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>

@endsection
